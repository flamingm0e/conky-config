# One of my conky configs #
  
Install the fonts if you want the icons.  

edit conky.sh to reflect your actual directory location.  
I have my conky.sh set as startup application  
  
in ./conkyrc/system you will find the actual conky config.  
currently set up for eth0/wlan0 <- change these to reflect your proper network devices.  
  
there are 2 scripts (gmail.sh and ip.sh)  
  
extra packages you might want to install for everything to mesh:  
vnstat  
lm-sensors  
  
![screenshot] (https://bitbucket.org/flamingm0e/conky-config/raw/db493b4eeed89b063a01540c488a65ef0c5af558/Workspace%201_012.png)